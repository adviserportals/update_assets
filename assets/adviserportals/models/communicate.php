<?php 
defined('C5_EXECUTE') or die(_("Access Denied."));

class messageList extends DatabaseItemList {
	
	public function setQuery($query) {
		$this->query = $query . ' ';
	}
		
}	

class communicate {
	
	public static function deleteMessage($msgId) {
		
		$db = Loader::db();
		## Sanitize it
		$msgId = filter_var($msgId, FILTER_SANITIZE_NUMBER_INT);
		
		## Delete messages
		$sql = "delete from pkgCommunicateMessages where msgStatus='sent' and msgID = ".$msgId;
		$delete = @$db->execute($sql);
		
		$sql = "delete from pkgCommunicateOpens where msgOpenMsgID = ".$msgId;
		$delete = @$db->execute($sql);
		
		$sql = "delete from pkgCommunicateClicks where msgClickMsgID = ".$msgId;
		$delete = @$db->execute($sql);
		
		$sql = "delete from pkgCommunicateUnsubscribes where msgUnsubscribeMsgID = ".$msgId;
		$delete = @$db->execute($sql);

		return;
		
	}
	
	public static function getDrafts() {
		
		$db = Loader::db();
		$sql = "select * from pkgCommunicateMessages where msgStatus='draft' order by msgID DESC;";
		
		$result = @$db->execute($sql);

		if(($result) && (!$result->EOF))	{

			while ($row = $result->FetchRow()) {

				$drafts[] = $row;	

			}
			
			return $drafts;

		} else {

			return false;

		}
		
	}	
	
	public static function getDraft($msgId) {
		
		$db = Loader::db();
		## Sanitize it
		$msgId = filter_var($msgId, FILTER_SANITIZE_NUMBER_INT);
		
		$sql = "select * from pkgCommunicateMessages where msgStatus='draft' and msgID=".$msgId.";";
		
		$result = @$db->execute($sql);

		if(($result) && (!$result->EOF))	{

			while ($row = $result->FetchRow()) {

				$drafts[] = $row;	

			}
			
			return $drafts;

		} else {

			return false;

		}
		
	}

	public static function saveAsDraft($groupList, $data) {
		
		$db = Loader::db();
		$groupS = serialize($groupList); 
		$msgSubject = filter_var(trim($data['messagesubject']), FILTER_SANITIZE_STRING);
		$msgBody = filter_var(trim($data['messagebody']), FILTER_SANITIZE_STRING);
		
		
		if($data['messageid']) {
			
			## This is a previously saved draft
			$sql = "update pkgCommunicateMessages set msgSendGroups = '".$groupS."', msgFrom = ".$data['messagefrom'].", msgSubject ='".$msgSubject."',";
			$sql.= "msgBody= '".$msgBody."', msgStatusDate= CURDATE() where msgID=".$data['messageid'].";";
			
			
		} else {	
			
			## This is a new message & first save
			$sql = "insert into pkgCommunicateMessages (msgSendGroups,msgSubscribers,msgFrom,msgSubject,msgBody,msgStatus,msgStatusDate) values (";
			$sql.= "'".$groupS."',0,".$data['messagefrom'].",'".$msgSubject."','".$msgBody."','draft',CURDATE());";
			
		}

		$saveAsDraft = @$db->Execute($sql); // @ to prevent error here, handle below
		
		return $saveAsDraft;
	
	}
	
	
	public static function saveAsSent($groupList, $data) {
		
		$db = Loader::db();
		$groupS = serialize($groupList); 
		$msgSubject = filter_var(trim($data['messagesubject']), FILTER_SANITIZE_STRING);
		$msgBody = filter_var(trim($data['messagebody']), FILTER_SANITIZE_STRING);
		
		$subscriberCount = self::countSubscribers($groupList);
		
		if(!$subscriberCount){
			
			$subscriberCount = 0;
		
		}	
		
		if($data['messageid']) {
			
			## This is a previously saved draft
			$sql = "update pkgCommunicateMessages set msgSendGroups = '".$groupS."', msgSubscribers = ".$subscriberCount.", ";
			$sql.= "msgFrom = ".$data['messagefrom'].", msgSubject ='".$msgSubject."', ";
			$sql.= "msgBody= '".$msgBody."', msgStatus = 'sent', msgStatusDate= CURDATE() where msgID=".$data['messageid'].";";
			
		} else {	
			
			## This is a new message & first save
			$sql = "insert into pkgCommunicateMessages (msgSendGroups,msgSubscribers,msgFrom,msgSubject,msgBody,msgStatus,msgStatusDate) values (";
			$sql.= "'".$groupS."',".$subscriberCount.",".$data['messagefrom'].",'".$msgSubject."','".$msgBody."','sent',CURDATE());";
			
		}

		$saveAsDraft = @$db->Execute($sql); // @ to prevent error here, handle below
		$tempMsgId = $db->Insert_ID();

		if($data['messageid'] != '') {
		
			$msgId = $data['messageid'];
			
		} else {
			
			$msgId = $tempMsgId;
			
		}		
		
		return $msgId;	
		
	}	
	
	private static function updateSentNumber($msgId,$sent) {
		
		$db = Loader::db();
		
		## Updates sent number when we know it
		$sql = "update pkgCommunicateMessages set msgSubscribers = ".$sent." ";
		$sql.= "where msgID=".$msgId.";";

		$updateSent = @$db->Execute($sql); // @ to prevent error here, handle below
		
		return;
		
	}
	
	private static function countSubscribers($groups) {
		
		Loader::model('search/group');
		Loader::model('user_list');
		$subscriberCount = 0;
		
		foreach($groups as $groupId) {
			
			$group = Group::getByID($groupId);
			$members = $group->getGroupMembers();
			
			foreach($members as $member) {
				
				if($member->getAttribute('communicate_subscriber') == 1) {
					
					$subscriberCount ++;
				
				}
				
			}	
					
		}

		return $subscriberCount;

	}	
	
	public static function getRecentSent() {
		
		$db = Loader::db();
		
		$sql = "select * from pkgCommunicateMessages left join (select msgOpenMsgID,count(msgOpenMsgID) as openCount ";
		$sql.= "from pkgCommunicateOpens group by msgOpenMsgID) as opens on msgID = msgOpenMsgID left join (select msgClickMsgID, ";
		$sql.= "count(msgClickID) as clickCount from pkgCommunicateClicks group by msgClickMsgID) as clicks on msgID = msgClickMsgID ";
		$sql.= "where msgStatus ='sent' order by msgID DESC limit 5";
		
		$result = @$db->execute($sql);

		if(($result) && (!$result->EOF))	{

			while ($row = $result->FetchRow()) {

				$recentSent[] = $row;	

			}
			
			return $recentSent;

		} else {

			return false;

		}
		
	}
	
	public static function getSent($limit) {
		
		$db = Loader::db();

		$sql = "select * from pkgCommunicateMessages where msgStatus ='sent' order by msgID DESC limit ".$limit.";";
		
		$result = @$db->execute($sql);

		if(($result) && (!$result->EOF))	{

			while ($row = $result->FetchRow()) {

				$sent[] = $row;	

			}
			
			return $sent;

		} else {

			return false;

		}
	
		
	}
	
	public static function getTotalSent() {
		
		$db = Loader::db();

		$sql = "select count(*) as totalSent from pkgCommunicateMessages where msgStatus ='sent';";
		
		$result = @$db->execute($sql);
		
		if(($result) && (!$result->EOF))	{

			while ($row = $result->FetchRow()) {
				
				$sent= $row['totalSent'];
			}
	
			return $sent;

		} else {

			return 0;

		}
		
	}
	
	public static function getTotalOpens() {
		
		$db = Loader::db();

		$sql = "select count(*) as totalSent from pkgCommunicateMessages where msgStatus ='sent';";
		
		$result = @$db->execute($sql);
		
		if(($result) && (!$result->EOF))	{

			while ($row = $result->FetchRow()) {
				
				$sent= $row['totalSent'];
			}
	
			return $sent;

		} else {

			return 0;

		}
		
	}
	
	public static function getMessageTotalSent($msgId) {
		
		$db = Loader::db();
		## Sanitize it
		$msgId = filter_var($msgId, FILTER_SANITIZE_NUMBER_INT);

		$sql = "select msgSubject,msgSubscribers as totalSent, msgStatusDate as dateSent from pkgCommunicateMessages where msgStatus ='sent' and msgID =".$msgId.";";
		
		$result = @$db->execute($sql);

		if(($result) && (!$result->EOF))	{

			while ($row = $result->FetchRow()) {

				$totalSent = $row;	

			}

			return $totalSent;

		} else {

			return 0;

		}
	
		
	}
	
	public static function getUniqueViews($msgId) {
		
		$db = Loader::db();
		## Sanitize it
		$msgId = filter_var($msgId, FILTER_SANITIZE_NUMBER_INT);

		$sql = "select count(distinct msgOpenUser) as uniqueOpens from pkgCommunicateOpens where msgOpenMsgID =".$msgId.";";
		
		$result = @$db->execute($sql);

		if(($result) && (!$result->EOF))	{

			while ($row = $result->FetchRow()) {

				$uniqueOpens = $row['uniqueOpens'];	

			}

			return $uniqueOpens;

		} else {

			return 0;

		}
		
	}
	
	public static function getAllViews($msgId) {
		
		$db = Loader::db();
		## Sanitize it
		$msgId = filter_var($msgId, FILTER_SANITIZE_NUMBER_INT);

		$sql = "select count(msgOpenUser) as allOpens from pkgCommunicateOpens where msgOpenMsgID =".$msgId.";";
		
		$result = @$db->execute($sql);

		if(($result) && (!$result->EOF))	{

			while ($row = $result->FetchRow()) {

				$allOpens = $row['allOpens'];	

			}

			return $allOpens;

		} else {

			return 0;

		}
		
	}
	
	public static function getOpenDetail($msgId,$limit) {
		
		$db = Loader::db();
		## Sanitize it
		$msgId = filter_var($msgId, FILTER_SANITIZE_NUMBER_INT);

		$sql = "select msgOpenUser, count(msgOpenMsgId) as totalOpens from pkgCommunicateOpens where msgOpenMsgID=".$msgId." group by msgOpenUser limit ".$limit.";";

		$result = @$db->execute($sql);

		if(($result) && (!$result->EOF))	{

			while ($row = $result->FetchRow()) {

				$opens[] = $row;	

			}
			
			return $opens;

		} else {

			return false;

		}
		
	}
	
	
	public static function getUniqueClicks($msgId) {
		
		$db = Loader::db();
		## Sanitize it
		$msgId = filter_var($msgId, FILTER_SANITIZE_NUMBER_INT);

		$sql = "select count(distinct msgClickUser) as uniqueClicks from pkgCommunicateClicks where msgClickMsgID =".$msgId.";";
		
		$result = @$db->execute($sql);

		if(($result) && (!$result->EOF))	{

			while ($row = $result->FetchRow()) {

				$uniqueClicks = $row['uniqueClicks'];	

			}

			return $uniqueClicks;

		} else {

			return 0;

		}
		
	}	
	
	public static function getAllClicks($msgId) {
		
		$db = Loader::db();
		## Sanitize it
		$msgId = filter_var($msgId, FILTER_SANITIZE_NUMBER_INT);

		$sql = "select count(msgClickUser) as allClicks from pkgCommunicateClicks where msgClickMsgID =".$msgId.";";
		
		$result = @$db->execute($sql);

		if(($result) && (!$result->EOF))	{

			while ($row = $result->FetchRow()) {

				$allClicks = $row['allClicks'];	

			}

			return $allClicks;

		} else {

			return 0;

		}
		
	}
	
	public static function getClickDetail($msgId,$limit) {
		
		$db = Loader::db();
		## Sanitize it
		$msgId = filter_var($msgId, FILTER_SANITIZE_NUMBER_INT);

		$sql = "select msgClickUser, count(msgClickMsgId) as totalClicks from pkgCommunicateClicks where msgClickMsgID=".$msgId." group by msgClickUser limit ".$limit.";";
		
		$result = @$db->execute($sql);

		if(($result) && (!$result->EOF))	{

			while ($row = $result->FetchRow()) {

				$clicks[] = $row;	

			}
			
			return $clicks;

		} else {

			return false;

		}
		
	}
	
	public static function getUniqueUnsubscribes($msgId) {
		
		$db = Loader::db();
		## Sanitize it
		$msgId = filter_var($msgId, FILTER_SANITIZE_NUMBER_INT);

		$sql = "select count(distinct msgUnsubscribeUser) as uniqueUnsubscribes from pkgCommunicateUnsubscribes where msgUnsubscribeMsgID =".$msgId.";";
		
		$result = @$db->execute($sql);

		if(($result) && (!$result->EOF))	{

			while ($row = $result->FetchRow()) {

				$uniqueUnsubscribes = $row['uniqueUnsubscribes'];	

			}

			return $uniqueUnsubscribes;

		} else {

			return 0;

		}
		
	}
	
	public static function getUnsubscribeDetail($msgId,$limit) {
		
		$db = Loader::db();
		## Sanitize it
		$msgId = filter_var($msgId, FILTER_SANITIZE_NUMBER_INT);

		$sql = "select distinct msgUnsubscribeUser from pkgCommunicateUnsubscribes where msgUnsubscribeMsgID=".$msgId." group by msgUnsubscribeUser limit ".$limit.";";
		
		$result = @$db->execute($sql);

		if(($result) && (!$result->EOF))	{

			while ($row = $result->FetchRow()) {

				$unsubscribes[] = $row;	

			}
			
			return $unsubscribes;

		} else {

			return false;

		}
		
	}
	
	public static function makeDraft($msgId) {
		
		$db = Loader::db();
		
		## This is a previously saved draft
		$sql = "update pkgCommunicateMessages set msgStatus = 'draft' where msgID=".$msgId.";";
		$updated = @$db->execute($sql);
		
	}	
	
	public static function sendMessage($msgId) {
		
		## Message tracking code
		$trackingCode = self::crypter($msgId);
		
		## Get Message
		$db = Loader::db();
		$sql = "select * from pkgCommunicateMessages where msgID=".$msgId.";";

		$result = @$db->execute($sql);

		if(($result) && (!$result->EOF))	{

			while ($row = $result->FetchRow()) {

				$messageBody = $row['msgBody'];
				$messageSubject = $row['msgSubject'];
				$messageFrom = $row['msgFrom'];	
				$messageGroups = unserialize($row['msgSendGroups']);	
						
			}

		} else {

			return false;
			
		}	
		
		$ui = UserInfo::getByID($messageFrom);
		$messageFromName = $ui->uName;
		$messageFromEmail = $ui->uEmail;	
		
		$siteUri = BASE_URL.DIR_REL;
		
		if(!URL_REWRITING) {
			
			$notPretty = '/index.php';
			
		} else {
			
			$notPretty = '';
			
		}		
		
		$txtOnlyTemplate = preg_replace('/<(.*?)>/','',$messageBody);
		
		## Image link replacement
		$imageLinkPattern = '/(?<=src=")(?!http:\/\/|\/\/)(\/.*?)(?=")/i';
	    $imageLinkReplacement =  $siteUri .'$1';
	    $messageBodyTemplate = preg_replace($imageLinkPattern, $imageLinkReplacement, $messageBody);
		
		## Get users and send
		Loader::model('search/group');
		Loader::model('user_list');
		$subscriberCount = 0;
		
		$sendList = array();
		
		foreach($messageGroups as $groupId) {
			
			$group = Group::getByID($groupId);
			$members = $group->getGroupMembers();
			
			foreach($members as $member) {
				
				if($member->getAttribute('communicate_subscriber') == 1) {
					
					$userCode=self::crypter($member->getUserID());
					$sendList[] = array(
										'userEmail' => $member->uEmail,
										'userCode' => $userCode
										);
				
				}
				
			}	
					
		}
		
		## Remove duplicates
		$sendList = self::arrayUnique($sendList);
		self::updateSentNumber($msgId,count($sendList));
				
		## Anchors
		$anchors = array();
		$anchorLinkPattern = '/href="(.*)">/';
		preg_match_all($anchorLinkPattern,$messageBody,$anchors);
		
		foreach($sendList as $send) {
			$messageBody = $messageBodyTemplate;
			$txtOnly = $txtOnlyTemplate;
			 
			$anchorLinkReplacement = '';
			
			 ## Anchor Link replacement
			foreach($anchors[1] as $anchor) {
			$replacer ='href="'.$anchor.'">';
	
			$anchorLinkReplacement =  'href="'.$siteUri.$notPretty.'/communicate/click/'.$trackingCode.'/'.$send['userCode'].'/'.urlencode(self::uriSafe($anchor)).'">';
			$messageBody = str_replace($replacer,$anchorLinkReplacement,$messageBody);

			}
			
			## Unsubscribe Link
			if(strpos($messageBody,'[unsubscribe]')) {
				
				## Replacement
				$messageBody = str_replace('[unsubscribe]','<a href="'.$siteUri.$notPretty.'/communicate/unsubscribe/'.$trackingCode.'/'.$send['userCode'].'">Unsubscribe</a>',$messageBody);
				$txtOnly = str_replace('[unsubscribe]','Click here to unsubscribe - '.$siteUri.$notPretty.'/communicate/unsubscribe/'.$trackingCode.'/'.$send['userCode'], $txtOnly);
			
			} else {
				
				## Hasn't been included add them
				$messageBody.='<br/><br/><p style="text-align:center;"><a href="'.$siteUri.$notPretty.'/communicate/unsubscribe/'.$trackingCode.'/'.$send['userCode'].'">Unsubscribe</a></p>';
				$txtOnly = $txtOnly.'<br/><br/>Click here to unsubscribe - '.$siteUri.$notPretty.'/communicate/unsubscribe/'.$trackingCode.'/'.$send['userCode'];
		
			}	
		
			## Add Tracking 
			$messageBody .= '<img src="'.$siteUri.$notPretty.'/communicate/stats/'.$trackingCode.'/'.$send['userCode'].'" height="1" width="1" />';
		
			## Mail it

			$mh = Loader::helper('mail');
			
			$mh->setSubject($messageSubject);
			$mh->setBody($txtOnly);
			$mh->setBodyHTML($messageBody);
			$mh->to($send['userEmail']);
			$mh->from($messageFromEmail,$messageFromName);
			$mh->sendMail();
		
		}
	
		return true;
		
	}	 
	
	public static function crypter($str, $decrypt = false) {
		
		define(A, '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz/');
		define(B, 'npcMkf4ti7PIRW9VNySea581Azs3KhqmLwvdYX6ZrjbECTlBFOQUguJGHx2Do0-');
		
		if(!$decrypt) {
			
			return strtr($str, A, B);
			
		} else {
		
			return strtr($str, B, A);
		}		

	}
	
	public static function uriSafe ($uri, $decrypt = false) {

		if(!$decrypt) {

			return rawurlencode($uri);

		} else {

			return rawurldecode($uri);
		}

	}
	
	public static function trackView($msgId,$userId) {
	
		$db = Loader::db();
		## Sanitize it
		$msgId = filter_var($msgId, FILTER_SANITIZE_NUMBER_INT);
		$userId = filter_var($userId, FILTER_SANITIZE_NUMBER_INT);
		
		## Valid message id
		$sql = "select msgId from pkgCommunicateMessages where msgId = ". $msgId;
		$valid = $db->execute($sql);
		
		if($valid) {
		
			$sql = "insert into pkgCommunicateOpens (msgOpenMsgID,msgOpenUser,msgOpenDate) values ($msgId,$userId,NOW());";
			$result = $db->execute($sql);
		
		}	
		
		return;
		
	}
	
	public static function trackClick($msgId,$userId,$uri) {
	
		$db = Loader::db();
		## Sanitize it
		$msgId = filter_var($msgId, FILTER_SANITIZE_NUMBER_INT);
		$userId = filter_var($userId, FILTER_SANITIZE_NUMBER_INT);
		$uri = mysql_real_escape_string($uri);
		
		## Valid message id
		$sql = "select msgId from pkgCommunicateMessages where msgId = ". $msgId;
		$valid = $db->execute($sql);
		
		if($valid) {
		
			$sql = "insert into pkgCommunicateClicks (msgClickMsgID,msgClickUser,msgClickUri,msgClickDate) values ($msgId,$userId,'".$uri."',NOW());";
			$result = $db->execute($sql);	
		
		}
		
		return;
		
	}

	public static function trackUnsubscribe($msgId,$userId) {
	
		$db = Loader::db();
		## Sanitize it
		$msgId = filter_var($msgId, FILTER_SANITIZE_NUMBER_INT);
		$userId = filter_var($userId, FILTER_SANITIZE_NUMBER_INT);
		
		## Valid message id
		$sql = "select msgId from pkgCommunicateMessages where msgId = ". $msgId;
		$valid = $db->execute($sql);
		
		if($valid) {
		
			$sql = "insert into pkgCommunicateUnsubscribes (msgUnsubscribeMsgID,msgUnsubscribeUser,msgUnsubscribeDate) values ($msgId,$userId,NOW());";
			$result = $db->execute($sql);	
		
		}
		
		return $result;
		
	}
	
	private static function arrayUnique($array, $preserveKeys = false) {
	    
		##Unique Array for return
	    $arrayRewrite = array();
	    
		##Array with the md5 hashes
	    $arrayHashes = array();
	    foreach($array as $key => $item) {
		
	        ## Serialize the current element and create a md5 hash
	        $hash = md5(serialize($item));
	        
			## If the md5 didn't come up yet, add the element to
	        ## to arrayRewrite, otherwise drop it
	        if (!isset($arrayHashes[$hash])) {
	            
				## Save the current element hash
	            $arrayHashes[$hash] = $hash;
	            
				## Add element to the unique Array
	            if ($preserveKeys) {
		
	                $arrayRewrite[$key] = $item;
	
	            } else {
		
	                $arrayRewrite[] = $item;
	
	            }
	        }
	    }
	
	    return $arrayRewrite;
	
	}
	
	
}
?>