<?php

/**
 * PHP Wrapper Class for Adviser Portals API
 * Version 1.0, supports GET (Read) only
 * This source file is subject to the GPL license
 *
 * @package 	Adviser Portals
 * @author 	Ollie Phillips <api@adviserportals.co.uk>
 * @copyright  	Copyright (c) 2012 Ollie Phillips
 * @license 	http://www.gnu.org/licenses/gpl.txt
 * @version 	2.0.1 08/08/2017
 * 
 * 2.0.1 Modified so newsletter downloads work under TLS
*/


class adviserPortalsApi {
	
	/**
	 * PHP request wrapper and class for Adviser Portals API
	 * 
	 * @author     Ollie Phillips <ollie@adviserportals.co.uk>
	 * @updated    Andy Griffiths <andy@adviserportals.co.uk>
	 * @version    2.0.1 08/08/2017
	 */
	
	## Change to your API key, available from http://www.adviserportals.co.uk
	protected $apiKey = '';
	
	## Set this to true if output is not as expected. Returns raw XML or Http response code if not '200 - OK'
	protected $debugMode = False;
	
	## Caching Options. Responses cached locally will improve performance.
	protected $cacheResponses = False; //True;
	protected $cacheExpireMinutes = 60;
	protected $cacheDirectory = 'cache';
	
	protected $apiLocation = 'http://www.adviserportals.co.uk/api';
	protected $apiVersion = '1.0'; 
	protected $requestUri;
	protected $exitSiteWarning;
	
	
	public function __construct() {
		
		/**
		 * Builds the requestUri
		 */
		
		$this->requestUri = $this->apiLocation.'/'.$this->apiVersion.'/';
				
	}	
	
	
	/**
	 * Research Links
	 */
	
	public function getResearchLinks($filter = Null) {
		
		/**
		 * Builds and initiates API request for Research Links
		 *
		 * @param integer/string $filter - Filters request. Single i.e 1, Range i.e '1-3' or Selection i.e '1,2,4' 
		 */
	
		$apiCall = 'researchlinks';
		
		if($filter) {
			
			$filter = '/researchID/'.$this->parseFilter($filter);
			
		}	
		
		$apiRequest = $this->requestUri.$apiCall.'/'.$this->apiKey.'/xml'.$filter;
		$apiResponse = $this->apiRequest($apiRequest);	
		
		if($apiResponse) {				
							
			$xml = simplexml_load_string($apiResponse);
			return $this->researchLinksView($xml);
		
		}
	
	}	
	
	
	protected function researchLinksView($xml) {
		
		/**
		 * Provides a HTML transformation of XML response for Research Links
		 * 
		 * @param object $xml - XML response object
		 */
		
		$this->exitSiteWarning = $xml->header->exitSiteStatement;
		$researchLinks ='<div id="ap-researchlinks-wrap">';
		
		foreach($xml->data->researchLink as $researchLink) {			
			$researchLinks.= '<div class="ap-researchlink">';
			$researchLinks.= '	<div class="ap-researchlink-thumbnail">';
			$researchLinks.= '		<a onclick="return apExitSiteWarning();" href="'.$researchLink->researchUri.'">';
			$researchLinks.= '			<img src="'.$researchLink->researchButtonUri.'" alt="'.$researchLink->researchTitle.'" />';
			$researchLinks.= '		</a>';
			$researchLinks.= '	</div>';
			$researchLinks.= '	<div class="ap-researchlinktext">';
			$researchLinks.= '		<div class="ap-researchlinktitlelink">';
			$researchLinks.= '			<h3>';
			$researchLinks.= '				<a onclick="return apExitSiteWarning();" href="'.$researchLink->researchUri.'" title="'.$researchLink->researchTitle.'">';
			$researchLinks.= 					$researchLink->researchTitle;
			$researchLinks.= '				</a>';
			$researchLinks.= '			</h3>';
			$researchLinks.= '		</div>';
			$researchLinks.= '		<div class="ap-researchlinkdescription">';
			$researchLinks.= 			$researchLink->researchDescription;
			$researchLinks.= '		</div>';
			$researchLinks.= '	</div>';
			$researchLinks.= '</div>';
		}
		
		$researchLinks.='</div>';
		$researchLinks.= $this->getExitSiteWarning();
		return $researchLinks;
		
	}	
	
	
	/**
	 * Plugins
	 */
	
	public function getPlugins($filter = Null) {
		
		/**
		 * Builds and initiates API request for Plugins
		 *
		 * @param integer/string $filter - Filters request. Single i.e 1, Range i.e '1-3' or Selection i.e '1,2,4' 
		 */
		
		$apiCall = 'plugins';
		
		if($filter) {

			$filter = '/pluginID/'.$this->parseFilter($filter);
			
		}	
		
		$apiRequest = $this->requestUri.$apiCall.'/'.$this->apiKey.'/xml'.$filter;
		$apiResponse = $this->apiRequest($apiRequest);
		
		if($apiResponse) {				
			
			$xml = simplexml_load_string($apiResponse);
			return $this->pluginsView($xml);				
		
		}
		
	}	
	
	
	protected function pluginsView($xml) {
		
		/**
		 * Provides a HTML transformation of XML response for Plugins
		 * 
		 * @param object $xml - XML response object
		 */
		
		$this->exitSiteWarning = $xml->header->exitSiteStatement;
		$plugins ='<div id="ap-plugins-wrap">';
		
		foreach($xml->data->plugin as $plugin) {
			$plugins.= '<div class="ap-plugin">';
			$plugins.= '	<div class="ap-plugin-thumbnail">';
			$plugins.= '		<a onclick="return apExitSiteWarning();" href="'.$plugin->pluginUri.$plugin->pluginClientAgencyReference.'">';
			$plugins.= '			<img src="'.$plugin->pluginButtonUri.'" alt="'.$plugin->pluginTitle.'" />';
			$plugins.= '		</a>';
			$plugins.= '	</div>';
			$plugins.= '	<div class="ap-plugin-text">';
			$plugins.= '		<div class="ap-plugin-titlelink">';
			$plugins.= '			<h3>';
			$plugins.= '				<a onclick="return apExitSiteWarning();" href="'.$plugin->pluginUri.$plugin->pluginClientAgencyReference.'" title="'.$plugin->pluginTitle.'">';
			$plugins.= 						$plugin->pluginTitle;
			$plugins.= '				</a>';
			$plugins.= '			</h3>';
			$plugins.= '		</div>';
			$plugins.= '		<div class="description">';
			$plugins.= 				$plugin->pluginDescription;
			$plugins.= '		</div>';
			$plugins.= '	</div>';
			$plugins.= '</div>';			
		}
		
		$plugins.='</div>';
		$plugins.= $this->getExitSiteWarning();
		return $plugins;
		
	}
	
	
	/**
	 * Wraps
	 */
	
	public function getWraps($filter = Null) {
		
		/**
		 * Builds and initiates API request for Wrap Platforms
		 *
		 * @param integer/string $filter - Filters request. Single i.e 1, Range i.e '1-3' or Selection i.e '1,2,4' 
		 */
	
		$apiCall = 'wraps';
		
		if($filter) {
			
			$filter = '/wrapID/'.$this->parseFilter($filter);
			
		}	
		
		$apiRequest = $this->requestUri.$apiCall.'/'.$this->apiKey.'/xml'.$filter;
		$apiResponse = $this->apiRequest($apiRequest);	
		
		if($apiResponse) {				
							
			$xml = simplexml_load_string($apiResponse);
			return $this->wrapsView($xml);
		
		}
	
	}	
	
	
	protected function wrapsView($xml) {
		
		/**
		 * Provides a HTML transformation of XML response for Wrap Platforms
		 * 
		 * @param object $xml - XML response object
		 */
		
		$this->exitSiteWarning = $xml->header->exitSiteStatement;
		$wraps ='<div id="ap-wraps-wrap">';
		
		foreach($xml->data->wrap as $wrap) {
			$wraps.= '<div class="ap-wrap">';
			$wraps.= '	<div class="ap-wrap-thumbnail">';
			$wraps.= '		<a onclick="return apExitSiteWarning();" href="'.$wrap->wrapUri.'">';
			$wraps.= '			<img src="'.$wrap->wrapButtonUri.'" alt="'.$wrap->wrapTitle.'" />';
			$wraps.= '		</a>';
			$wraps.= '	</div>';
			$wraps.= '	<div class="ap-wrap-text">';
			$wraps.= '		<div class="ap-wrap-titlelink">';
			$wraps.= '			<h3>';
			$wraps.= '				<a onclick="return apExitSiteWarning();" href="'.$wrap->wrapUri.'" title="'.$wrap->wrapTitle.'">';
			$wraps.= 					$wrap->wrapTitle;
			$wraps.= '				</a>';
			$wraps.= '			</h3>';
			$wraps.= '		</div>';
			$wraps.= '		<div class="ap-wrap-description">';
			$wraps.= 			$wrap->wrapDescription;
			$wraps.= '		</div>';
			$wraps.= '	</div>';
			$wraps.= '</div>';
		}
		
		$wraps.='</div>';
		$wraps.= $this->getExitSiteWarning();
		return $wraps;
		
	}
	
		
	/**
	 * Newsletters
	 */
	
	public function getNewsletters($filter = Null) {
		
		/**
		 * Builds and initiates API request for Newsletters
		 * Checks $_REQUEST to determine whether to show all newsletters or download a specific newsletter
		 *
		 * @param integer/string $filter - Filters request. Single i.e 1, Range i.e '1-3' or Selection i.e '1,2,4' 
		 */
		
		if($_REQUEST) {
		
			if ($_REQUEST['title'] && $_REQUEST['id'] && $_REQUEST['key']) {
				
				$this->newsletterDownload($_REQUEST['id']);
				
			}
	
		}		  
			
		$apiCall = 'newsletters';
	
		if($filter) {
		
			$filter = '/newsletterID/'.$this->parseFilter($filter);
		
		}	
	
		$apiRequest = $this->requestUri.$apiCall.'/'.$this->apiKey.'/xml'.$filter;
		$apiResponse = $this->apiRequest($apiRequest);
	
		if($apiResponse) {				
		
			$xml = simplexml_load_string($apiResponse);
			return $this->newslettersView($xml);				
	
		}	
		
	}	
	
	
	public function newsletterDownload($filter){
		
		/**
		 * Builds and initiates API request for Newsletter download
		 *
		 * @param integer $filter - ID of newsletter to download
		 */
		
		$apiCall = 'newsletter';

		if($filter) {

			$filter = '/newsletterID/'.$filter;
			$apiRequest = $this->requestUri.$apiCall.'/'.$this->apiKey.'/xml'.$filter;
			//echo '<iframe src="'.$apiRequest.'" width="1" height="1" border="0" style="visibility:hidden;"></iframe>';
			echo '<script>window.open("'.$apiRequest.'", "_blank", "width=370,height=100");</script>';
		}	
	
	}	
	
	
	protected function newslettersView($xml) {
		
		/**
		 * Provides a HTML transformation of XML response for Newsletters
		 * 
		 * @param object $xml - XML response object
		 */
		
		$newsletterIconUri = 'https://e82ad80c8d0c6a6e05d5-a83e20f0f3ac69df7b633e2d849af628.ssl.cf3.rackcdn.com';
		$newsletters ='<div id="ap-newsletters-wrap">';
		$newsletters .= '<p style="font-weight:bold;">Please note, you may need to allow pop-ups in your browser to download newletters</p>';
		$request = explode('?',$_SERVER['REQUEST_URI']);
		$scriptName = $request[0];
		
		foreach($xml->data->newsletter as $newsletter) {			
			$newsletters.= '<div class="ap-newsletter">';
			$newsletters.= '	<div class="ap-newsletter-thumbnail">';
			$newsletters.= '		<a rel="noindex" href="'.$scriptName.'?title='.urlencode($newsletter->newsletterTitle).'&id='.$newsletter->newsletterID.'&key='.$this->apiKey.'">';
			$newsletters.= '			<img src="'.$newsletterIconUri.'/the_adviser_icon.png" alt="'.$newsletter->newsletterTitle.'" />';
			$newsletters.= '		</a>';
			$newsletters.= '	</div>';
			$newsletters.= '	<div class="ap-newsletter-text">';
			$newsletters.= '		<div class="ap-newsletter-titlelink">';
			$newsletters.= '			<h3>';
			$newsletters.= '				<a rel="noindex" href="'.$scriptName.'?title='.urlencode($newsletter->newsletterTitle).'&id='.$newsletter->newsletterID.'&key='.$this->apiKey.'">';
			$newsletters.= 						$newsletter->newsletterTitle;
			$newsletters.= '				</a>';
			$newsletters.= '			</h3>';
			$newsletters.= '			<h4>';
			$newsletters.= 					$newsletter->newsletterEdition;
			$newsletters.= '			</h4>';
			$newsletters.= '		</div>';
			$newsletters.= '		<div class="ap-newsletter-description">';
			$newsletters.= 				$newsletter->newsletterDescription;
			$newsletters.= '		</div>';
			$newsletters.= '	</div>';
			$newsletters.= '</div>';			
		}
		
		$newsletters.='</div>';
		return $newsletters;
		
	}
	
	
	/**
	 * Newsfeeds
	 */
	
	public function getNewsfeeds($filter = Null) {
		
		/**
		 * Builds and initiates API request for Newsfeeds
		 * Checks $_REQUEST to determine how to show Newsfeeds
		 *
		 * @param integer/string $filter - Filters request. Single i.e 1, Range i.e '1-3' or Selection i.e '1,2,4' 
		 */
		
		if($_REQUEST['title'] && $_REQUEST['id']) {
		
			return $this->getNewsfeed($_REQUEST['id']);
				
		} else {
		
			$apiCall = 'newsfeeds';
		
			if($filter) {
			
				$filter = '/newsfeedID/'.$this->parseFilter($filter);
			
			}	
		
			$apiRequest = $this->requestUri.$apiCall.'/'.$this->apiKey.'/xml'.$filter;
			$apiResponse = $this->apiRequest($apiRequest);
		
			if($apiResponse) {				
			
				$xml = simplexml_load_string($apiResponse);
				return $this->newsfeedsView($xml);				
		
			}
		
		}	
			
	}
	
	
	protected function newsfeedsView($xml) {
		
		/**
		 * Provides a HTML transformation of XML response for Newsfeeds
		 * 
		 * @param object $xml - XML response object
		 */
		
		$newsfeeds ='<div id="ap-newsfeeds-wrap">';
		
		$request = explode('?',$_SERVER['REQUEST_URI']);
		$scriptName = $request[0];
		
		foreach($xml->data->newsfeed as $newsfeed) {
			$newsfeeds.= '<div class="ap-newsfeed">';
			$newsfeeds.= '	<div class="ap-newsfeed-thumbnail">';
			$newsfeeds.= '		<a href="'.$scriptName.'?title='.urlencode($newsfeed->newsfeedTitle).'&id='.$newsfeed->newsfeedID.'">';
			$newsfeeds.= '			<img src="'.$newsfeed->newsfeedButtonUri.'" alt="'.$newsfeed->newsfeedTitle.'" />';
			$newsfeeds.= '		</a>';
			$newsfeeds.= '	</div>';
			$newsfeeds.= '	<div class="ap-newsfeed-text">';
			$newsfeeds.= '		<div class="ap-newsfeed-titlelink">';
			$newsfeeds.= '			<h3>';
			$newsfeeds.= '				<a href="'.$scriptName.'?title='.urlencode($newsfeed->newsfeedTitle).'&id='.$newsfeed->newsfeedID.'">'.$newsfeed->newsfeedTitle.'</a>';
			$newsfeeds.= '			</h3>';
			$newsfeeds.= '			<h4>';
			$newsfeeds.= 				$newsfeed->newsfeedCategory;
			$newsfeeds.= '			</h4>';
			$newsfeeds.= '		</div>';
			$newsfeeds.= '		<div class="ap-newsfeed-description">';
			$newsfeeds.= 			$newsfeed->newsfeedDescription;
			$newsfeeds.= '		</div>';
			$newsfeeds.= '	</div>';
			$newsfeeds.= '</div>';
		}
		
		$newsfeeds.='</div>';
		return $newsfeeds;
		
	}
	
	
	/**
	 * Newsfeed
	 */
	
	public function getNewsfeed($filter = Null, $items = 5) {
		
		/**
		 * Builds and initiates API request for Newsfeed
		 *
		 * @param integer/string $filter - Filters request. Single i.e 1, Range i.e '1-3' or Selection i.e '1,2,4' 
		 * @param integer $items - Number of items to display from Newsfeed. Default is 5
		 */
		
		$apiCall = 'newsfeed';
	
		if($filter) {
		
			$filter = '/newsfeedID/'.$this->parseFilter($filter);
		
		}	
	
		$apiRequest = $this->requestUri.$apiCall.'/'.$this->apiKey.'/xml'.$filter;
		$apiResponse = $this->apiRequest($apiRequest);
	
		if($apiResponse) {				
		
			$xml = simplexml_load_string($apiResponse);
			return $this->newsfeedParser($xml, $items);				
	
		}
				
	}	
	
	
	protected function newsfeedParser($xml, $items) {
		
		/**
		 * Parses requested Newsfeed
		 *
		 * @param object $xml - XML response object
		 * @param integer $items - Number of items to display from Newsfeed
		 */
		
		$this->exitSiteWarning = $xml->header->exitSiteStatement;
		
		$itemsArray = array();
		foreach($xml->data->newsfeed as $newsfeed) {
			
			$newsfeedRss = $this->newsfeedRequest($newsfeed->newsfeedUri);
			
			if($newsfeedRss) {
			$itemCount = 0;
				$rss = simplexml_load_string($newsfeedRss);
				foreach ($rss->channel->item as $item) {
				
					if($itemCount < $items) {
					
						$itemsArray[] = array('title'=> $item->title,'description'=> preg_replace("/<img[^>]+\>/i", "", $item->description), 'link'=>$item->link,'category'=>$newsfeed->newsfeedCategory);
						$itemCount++;
					
					}	
					
				}	
			
			}	
				
		}	
		
		return $this->newsfeedItemsView($itemsArray);
			
	}	
	
	
	protected function newsfeedItemsView($items) {
		
		/**
		 * Provides a HTML transformation of Newsfeed items
		 * 
		 * @param array $items - Newsfeed items
		 */
		
		$strReplace = array('<br/>');
		
		$newsfeedItems ='<div id="ap-newsfeeditem-wrap">';
		$itemCategory = '';
		
		foreach($items as $key => $value) {
				
				$newsfeedItems.= '<div class="ap-newsfeeditem">';
				
				$curItemCategory = $value['category'];
				if(strtolower(trim($curItemCategory)) != strtolower(trim($itemCategory))) {
					$newsfeedItems .= '<div class="ap-newsfeeditem-category">';
					$newsfeedItems .= '	<h4>';
					$newsfeedItems .= 		$curItemCategory;
					$newsfeedItems .= '	</h4>';
					$newsfeedItems .= '</div>';
					$itemCategory = $curItemCategory;
				}
				
				$newsfeedItems.= '	<div class="ap-newsfeeditem-text">';
				$newsfeedItems.= '		<div class="ap-newsfeeditem-titlelink">';
				$newsfeedItems.= '			<h5>';
				$newsfeedItems.= '				<a onclick="return apExitSiteWarning();" href="'.$value['link'].'" title="'.$value['title'].'">';
				$newsfeedItems.= 					$value['title'];
				$newsfeedItems.= '				</a>';
				$newsfeedItems.= '			</h5>';
				$newsfeedItems.= '		</div>';
				$newsfeedItems.= '		<div class="ap-newsfeeditem-description">';
				$newsfeedItems.= 			str_replace($strReplace,'', $value['description']);
				$newsfeedItems.= '		</div>';
				$newsfeedItems.= '	</div>';
				$newsfeedItems.= '</div>';
				
		}
		
		$newsfeedItems.='</div>';
		$newsfeedItems.= $this->getExitSiteWarning();
		return $newsfeedItems;
				
	}	
	
	
	/**
	 * Calculators
	 */
	
	public function getCalculators($filter = Null) {
		
		/**
		 * Builds and initiates API request for Calculators
		 * Checks $_REQUEST to determine whether to show all calculators or a specific calculator
		 *
		 * @param integer/string $filter - Filters request. Single i.e 1, Range i.e '1-3' or Selection i.e '1,2,4' 
		 */
		
		if($_REQUEST['title'] && $_REQUEST['id']) {
		
			return $this->getCalculator($_REQUEST['id']);
				
		} else {
		
			$apiCall = 'calculators';
		
			if($filter) {
			
				$filter = '/calculatorID/'.$this->parseFilter($filter);
			
			}	
		
			$apiRequest = $this->requestUri.$apiCall.'/'.$this->apiKey.'/xml'.$filter;
			$apiResponse = $this->apiRequest($apiRequest);
		
			if($apiResponse) {				
			
				$xml = simplexml_load_string($apiResponse);
				return $this->calculatorsView($xml);				
		
			}
		
		}	
			
	}
	
	
	protected function calculatorsView($xml) {
		
		/**
		 * Provides a HTML transformation of XML response for Calculators
		 * 
		 * @param object $xml - XML response object
		 */
		
		$calculators ='<div id="ap-calculators-wrap">';
		
		$request = explode('?',$_SERVER['REQUEST_URI']);
		$scriptName = $request[0];
		
		foreach($xml->data->calculator as $calculator) {
			$calculators.= '<div class="ap-calculator">';
			$calculators.= '	<div class="ap-calculator-thumbnail">';
			$calculators.= '		<a href="'.$scriptName.'?title='.urlencode($calculator->calculatorTitle).'&id='.$calculator->calculatorID.'">';
			$calculators.= '			<img src="'.$calculator->calculatorButtonUri.'" alt="'.$calculator->calculatorTitle.'" />';
			$calculators.= '		</a>';
			$calculators.= '	</div>';
			$calculators.= '	<div class="ap-calculator-text">';
			$calculators.= '		<div class="ap-calculator-titlelink">';
			$calculators.= '			<h3>';
			$calculators.= '				<a href="'.$scriptName.'?title='.urlencode($calculator->calculatorTitle).'&id='.$calculator->calculatorID.'">';
			$calculators.= 						$calculator->calculatorTitle;
			$calculators.= '				</a>';
			$calculators.= '			</h3>';
			$calculators.= '		</div>';
			$calculators.= '		<div class="ap-calculator-description">';
			$calculators.= 				$calculator->calculatorDescription;
			$calculators.= '		</div>';
			$calculators.= '	</div>';
			$calculators.= '</div>';
		}
		
		$calculators.='</div>';
		return $calculators;
		
	}
	
	
	/**
	 * Calculator
	 */
	
	public function getCalculator($filter) {
		
		/**
		 * Builds and initiates API request for Calculator
		 *
		 * @param integer $filter - ID of calculator to display
		 */
		
		$apiCall = 'calculator';
	
		if($filter) {
		
			$filter = '/calculatorID/'.$filter;
		
		}	
	
		$apiRequest = $this->requestUri.$apiCall.'/'.$this->apiKey.'/xml'.$filter;
		$apiResponse = $this->apiRequest($apiRequest);
	
		if($apiResponse) {				
		
			$xml = simplexml_load_string($apiResponse);
			return $this->calculatorView($xml);				
	
		}
				
	}
	
	
	protected function calculatorView($xml) {	
		
		/**
		 * Provides a HTML transformation of XML response for Calculator
		 * 
		 * @param object $xml - XML response object
		 */	

		$calculatorItem ='<div class="ap-calculator-item-wrap">';
		
		foreach($xml->data->calculator as $calculator) {
			$calculatorItem.= '<div class="ap-calculator-item">';
			$calculatorItem.= $calculator->calculatorSyntax;
			$calculatorItem.= '</div>';
		}
		
		$calculatorItem.='</div>';
		
		return $calculatorItem;
		
	}
	
	
	/**
	 * Videos
	 */
	
	public function getVideos($filter = Null) {
		
		/**
		 * Builds and initiates API request for Videos
		 * Checks $_REQUEST to determine whether to show all videos or a specific video
		 *
		 * @param integer/string $filter - Filters request. Single i.e 1, Range i.e '1-3' or Selection i.e '1,2,4' 
		 */
		
		if($_REQUEST['title'] && $_REQUEST['id']) {
		
			return $this->getVideo($_REQUEST['id']);
				
		} else {
		
			$apiCall = 'videos';
		
			if($filter) {
			
				$filter = '/videoID/'.$this->parseFilter($filter);
			
			}	
		
			$apiRequest = $this->requestUri.$apiCall.'/'.$this->apiKey.'/xml'.$filter;
			$apiResponse = $this->apiRequest($apiRequest);
		
			if($apiResponse) {				
			
				$xml = simplexml_load_string($apiResponse);
				return $this->videosView($xml);				
		
			}
		
		}	
			
	}
	
	
	protected function videosView($xml) {
		
		/**
		 * Provides a HTML transformation of XML response for Videos
		 * 
		 * @param object $xml - XML response object
		 */
		
		$videos ='<div id="ap-videos-wrap">';
		
		$request = explode('?',$_SERVER['REQUEST_URI']);
		$scriptName = $request[0];
		
		foreach($xml->data->video as $video) {
			$videos.= '<div class="ap-video">';
			$videos.= '	<div class="ap-video-thumbnail">';
			$videos.= '		<a href="'.$scriptName.'?title='.urlencode($video->videoTitle).'&id='.$video->videoID.'">';
			$videos.= '			<img src="'.$video->videoButtonUri.'" alt="'.$video->videoTitle.'" />';
			$videos.= '		</a>';
			$videos.= '	</div>';
			$videos.= '	<div class="ap-video-text">';
			$videos.= '		<div class="ap-video-titlelink">';
			$videos.= '			<h3>';
			$videos.= '				<a href="'.$scriptName.'?title='.urlencode($video->videoTitle).'&id='.$video->videoID.'">';
			$videos.= 					$video->videoTitle;
			$videos.= '				</a>';
			$videos.= '			</h3>';
			$videos.= '		</div>';
			$videos.= '		<div class="ap-video-description">';
			$videos.= 			$video->videoDescription;
			$videos.= '		</div>';
			$videos.= '	</div>';
			$videos.= '</div>';
		}
		
		$videos.='</div>';
		return $videos;
		
	}
	
	
	/**
	 * Video
	 */
	
	public function getVideo($filter) {
		
		/**
		 * Builds and initiates API request for Video
		 *
		 * @param integer $filter - ID of video to display
		 */
		
		$apiCall = 'video';
	
		if($filter) {
		
			$filter = '/videoID/'.$filter;
		
		}	
	
		$apiRequest = $this->requestUri.$apiCall.'/'.$this->apiKey.'/xml'.$filter;
		$apiResponse = $this->apiRequest($apiRequest);
	
		if($apiResponse) {				
		
			$xml = simplexml_load_string($apiResponse);
			return $this->videoView($xml);				
	
		}
				
	}
	
	
	protected function videoView($xml) {	
		
		/**
		 * Provides a HTML transformation of XML response for Video
		 * 
		 * @param object $xml - XML response object
		 */	

		$videoItem ='<div id="ap-videoitem-wrap">';
		
		foreach($xml->data->video as $video) {
			
			$videoItem.= '<div class="ap-videoitem">';
			$videoItem.= $video->videoSyntax;
			$videoItem.= '</div>';
			
		}
		
		$videoItem.='</div>';
		
		return $videoItem;
		
	}
	
	
	/**
	 * Forms
	 */
	
	public function getForms($filter = Null) {
		
		/**
		 * Builds and initiates API request for Forms
		 * Checks $_REQUEST to determine whether to show all forms or a specific form
		 *
		 * @param integer/string $filter - Filters request. Single i.e 1, Range i.e '1-3' or Selection i.e '1,2,4' 
		 */
		
		if($_REQUEST['title'] && $_REQUEST['id']) {
		
			return $this->getForm($_REQUEST['id']);
				
		} else {
		
			$apiCall = 'forms';
		
			if($filter) {
			
				$filter = '/formID/'.$this->parseFilter($filter);
			
			}	
		
			$apiRequest = $this->requestUri.$apiCall.'/'.$this->apiKey.'/xml'.$filter;
			$apiResponse = $this->apiRequest($apiRequest);
		
			if($apiResponse) {				
			
				$xml = simplexml_load_string($apiResponse);
				return $this->formsView($xml);				
		
			}
		
		}	
			
	}
	
	
	protected function formsView($xml) {
		
		/**
		 * Provides a HTML transformation of XML response for Forms
		 * 
		 * @param object $xml - XML response object
		 */
		
		$forms ='<div id="ap-forms-wrap">';
		
		$request = explode('?',$_SERVER['REQUEST_URI']);
		$scriptName = $request[0];
		
		foreach($xml->data->form as $form) {
			$forms.= '<div class="ap-form">';
			$forms.= '	<div class="ap-form-thumbnail">';
			$forms.= '		<a href="'.$scriptName.'?title='.urlencode($form->formTitle).'&id='.$form->formID.'">';
			$forms.= '			<img src="'.$form->formButtonUri.'" alt="'.$form->formTitle.'" />';
			$forms.= '		</a>';
			$forms.= '	</div>';
			$forms.= '</div>';
		}
		
		$forms.='</div>';
		return $forms;
		
	}
	
	
	/**
	 * Form
	 */
	
	public function getForm($filter) {
		
		/**
		 * Builds and initiates API request for Form
		 *
		 * @param integer $filter - ID of form to display
		 */
		
		$apiCall = 'form';
	
		if($filter) {
		
			$filter = '/formID/'.$filter;
		
		}	
	
		$apiRequest = $this->requestUri.$apiCall.'/'.$this->apiKey.'/xml'.$filter;
		$apiResponse = $this->apiRequest($apiRequest);
	
		if($apiResponse) {				
		
			$xml = simplexml_load_string($apiResponse);
			return $this->formView($xml);				
	
		}
				
	}
	
	
	protected function formView($xml) {	
		
		/**
		 * Provides a HTML transformation of XML response for Form
		 * 
		 * @param object $xml - XML response object
		 */	

		$formItem ='<div id="ap-formitem-wrap">';
		
		foreach($xml->data->form as $form) {
			$formItem.= '<div class="ap-formitem">';
			$formItem.= $form->formSyntax;
			$formItem.= '</div>';
			//$formItem.= '<div class="ap-formmobile">To use this form, please visit our site on a device with a larger screen size or visit the \'Contact Us\' section of our website.</div>';
		}
		
		$formItem.='</div>';
		
		return $formItem;
		
	}
	

	/**
	 * Market Data
	 */
		
	public function getMarketIndices(){
		
		/**
		 * Builds and initiates API request for all Market Data - Indices
		 */

		$apiCall = 'marketindices';

		$apiRequest = $this->requestUri.$apiCall.'/'.$this->apiKey.'/xml';
		$apiResponse = $this->apiRequest($apiRequest, False);

		if($apiResponse) {				

			$xml = simplexml_load_string($apiResponse);
			return $this->marketindicesView($xml);				

		}

	}
	

	public function getMarketWinners(){
		
		/**
		 * Builds and initiates API request for all Market Data - Winners
		 */

		$apiCall = 'marketwinners';

		$apiRequest = $this->requestUri.$apiCall.'/'.$this->apiKey.'/xml';
		$apiResponse = $this->apiRequest($apiRequest, False);

		if($apiResponse) {				

			$xml = simplexml_load_string($apiResponse);
			return $this->marketwinnersView($xml);				

		}

	}


	public function getMarketLosers(){
		
		/**
		 * Builds and initiates API request for all Market Data - Losers
		 */

		$apiCall = 'marketlosers';

		$apiRequest = $this->requestUri.$apiCall.'/'.$this->apiKey.'/xml';
		$apiResponse = $this->apiRequest($apiRequest, False);

		if($apiResponse) {				

			$xml = simplexml_load_string($apiResponse);
			return $this->marketlosersView($xml);				

		}

	}	
	

	public function getMarketCurrencies(){
		
		/**
		 * Builds and initiates API request for all Market Data - Currencies
		 */

		$apiCall = 'marketcurrencies';

		$apiRequest = $this->requestUri.$apiCall.'/'.$this->apiKey.'/xml';
		$apiResponse = $this->apiRequest($apiRequest, False);

		if($apiResponse) {				

			$xml = simplexml_load_string($apiResponse);
			return $this->marketcurrenciesView($xml);				

		}

	}
	

	public function getMarketSummary(){
		
		/**
		 * Builds and initiates API request for all Market Data - Summary
		 */

		$apiCall = 'marketsummary';

		$apiRequest = $this->requestUri.$apiCall.'/'.$this->apiKey.'/xml';
		$apiResponse = $this->apiRequest($apiRequest, False);

		if($apiResponse) {				

			$xml = simplexml_load_string($apiResponse);
			return $this->marketsummaryView($xml);				

		}

	}
	
	
	protected function marketindicesView($xml) {
		
		/**
		 * Provides a HTML transformation of XML response for Market Indices
		 * 
		 * @param object $xml - XML response object
		 */	
			
		$marketData ='<div id="ap-marketindices-wrap">';
		
		foreach($xml->data->indices as $indices) {
			
			$marketData.= $indices;
		
		}
		
		$marketData.='</div>';
		return $marketData;
		
	}
	
	
	protected function marketwinnersView($xml) {
		
		/**
		 * Provides a HTML transformation of XML response for Market Winners
		 * 
		 * @param object $xml - XML response object
		 */	
			
		$marketData ='<div id="ap-marketwinners-wrap">';
		
		foreach($xml->data->winners as $winners) {
			
			$marketData.= $winners;
		
		}
		
		$marketData.='</div>';
		return $marketData;
		
	}
	
	
	protected function marketlosersView($xml) {	
		
		/**
		 * Provides a HTML transformation of XML response for Market Losers
		 * 
		 * @param object $xml - XML response object
		 */
			
		$marketData ='<div id="ap-marketlosers-wrap">';
		
		foreach($xml->data->losers as $losers) {
			
			$marketData.= $losers;
		
		}
		
		$marketData.='</div>';
		return $marketData;
		
	}
	
	
	protected function marketcurrenciesView($xml) {	
		
		/**
		 * Provides a HTML transformation of XML response for Market Currencies
		 * 
		 * @param object $xml - XML response object
		 */
			
		$marketData ='<div id="ap-currencymarket-wrap">';
		
		foreach($xml->data->currencies as $currency) {
			
			$marketData.= $currency;
		
		}
		
		$marketData.='</div>';
		return $marketData;
		
	}


	protected function marketsummaryView($xml) {
		
		/**
		 * Provides a HTML transformation of XML response for Market Summary
		 * 
		 * @param object $xml - XML response object
		 */	
			
		$marketData ='<div id="ap-marketsummary-wrap">';
		
		foreach($xml->data->summary as $marketSummary) {
			
			$marketData.= $marketSummary;
		
		}
		
		$marketData.='</div>';
		return $marketData;
		
	}
	
	
	/**
	 * Article Categories
	 */
	
	public function getArticleCategories($filter = Null) {
		
		/**
		 * Builds and initiates API request for Article Categories
		 *
		 * @param integer/string $filter - Filters request. Single i.e 1, Range i.e '1-3' or Selection i.e '1,2,4' 
		 */
		
		if($_REQUEST['acid'] || $_REQUEST['aid']) {
			
			// Articles By Category
			if(isset($_REQUEST['acid'])) {
		
				if ($_REQUEST['title'] && $_REQUEST['acid']) {
				
					return $this->getArticlesByCategoryId($_REQUEST['acid'],True);
					
				}
			
			}
			
			// Article
			if(isset($_REQUEST['aid'])) {
		
				if ($_REQUEST['title'] && $_REQUEST['aid']) {
				
					return $this->getArticle($_REQUEST['aid']);
				
				}
			
			}	
	
		} else {
		
			$apiCall = 'articlecategories';
			
			if($filter) {
			
				$filter = '/articleCategoryID/'.$this->parseFilter($filter);
			
			}	
		
			$apiRequest = $this->requestUri.$apiCall.'/'.$this->apiKey.'/xml'.$filter;
			$apiResponse = $this->apiRequest($apiRequest);
		
			if($apiResponse) {				
			
				$xml = simplexml_load_string($apiResponse);
				return $this->articlecategoriesView($xml);				
		
			}
		
		}	
			
	}
	
	
	protected function articlecategoriesView($xml) {
		
		/**
		 * Provides a HTML transformation of XML response for Article Categories
		 * 
		 * @param object $xml - XML response object
		 */
		
		$articleCategories ='<div id="ap-articlecategories-wrap">';
		
		$request = explode('?',$_SERVER['REQUEST_URI']);
		$scriptName = $request[0];
		
		foreach($xml->data->articleCategory as $articleCategory) {
			$articleCategories.= '<div class="ap-articlecategory">';
			$articleCategories.= '	<div class="ap-articlecategory-thumbnail">';
			$articleCategories.= '		<a href="'.$scriptName.'?title='.urlencode($articleCategory->articleCategoryName).'&acid='.$articleCategory->articleCategoryID.'">';
			$articleCategories.= '			<img src="'.$articleCategory->articleCategoryThumbnail.'" alt="'.$articleCategory->articleCategoryName.'" />';
			$articleCategories.= '		</a>';
			$articleCategories.= '	</div>';
			$articleCategories.= '	<div class="ap-articlecategory-text">';
			$articleCategories.= '		<div class="ap-articlecategory-titlelink">';
			$articleCategories.= '			<h3>';
			$articleCategories.= '				<a href="'.$scriptName.'?title='.urlencode($articleCategory->articleCategoryName).'&acid='.$articleCategory->articleCategoryID.'">';
			$articleCategories.= 					$articleCategory->articleCategoryName;
			$articleCategories.= '				</a>';
			$articleCategories.= '			</h3>';
			$articleCategories.= '		</div>';
			$articleCategories.= '		<div class="ap-articlecategory-description">';
			$articleCategories.= '			<p>';
			$articleCategories.= 				$articleCategory->articleCategoryDescription;
			$articleCategories.= '			</p>';
			$articleCategories.= '		</div>';
			$articleCategories.= '	</div>';
			$articleCategories.= '</div>';
		}
		
		$articleCategories.='</div>';
		return $articleCategories;
		
	}


	/**
	 * Articles
	 */
	
	public function getArticles($filter = Null,$ignoreRequest = False) {
		
		/**
		 * Builds and initiates API request for Articles
		 *
		 * @param integer/string $filter - Filters request. Single i.e 1, Range i.e '1-3' or Selection i.e '1,2,4' 
		 * @param boolean $ignoreRequest - Used internally to prevent looking at $_REQUEST in some circumstances
		 */
		
		if($_REQUEST['title'] && $_REQUEST['aid'] && !$ignoreRequest) {
		
			return $this->getArticle($_REQUEST['aid']);
	
		} else {
		
			$apiCall = 'articles';
	
			if($filter) {
	
				$filter = '/articleID/'.$this->parseFilter($filter);
	
			}	

			$apiRequest = $this->requestUri.$apiCall.'/'.$this->apiKey.'/xml'.$filter;
	
			$apiResponse = $this->apiRequest($apiRequest);
	
			if($apiResponse) {				
	
				$xml = simplexml_load_string($apiResponse);
				return $this->articlesView($xml);				

			}
		
		}
			
	}
	
	
	public function getArticlesByCategoryId($filter = Null,$ignoreRequest = False) {
		
		/**
		 * Builds and initiates API request for Articles by articleCategoryID
		 *
		 * @param integer/string $filter - Filters request. Single i.e 1, Range i.e '1-3' or Selection i.e '1,2,4' 
		 * @param boolean $ignoreRequest - Used internally to prevent looking at $_REQUEST in some circumstances
		 */
		
		if($_REQUEST['title'] && $_REQUEST['aid'] && !$ignoreRequest) {
		
			return $this->getArticle($_REQUEST['aid']);
	
		} else {
		
			$apiCall = 'articles';
	
			if($filter) {
	
				$filter = '/articleCategoryID/'.$this->parseFilter($filter);
	
			}	

			$apiRequest = $this->requestUri.$apiCall.'/'.$this->apiKey.'/xml'.$filter;
	
			$apiResponse = $this->apiRequest($apiRequest);
	
			if($apiResponse) {				
	
				$xml = simplexml_load_string($apiResponse);
				return $this->articlesView($xml);				

			}
		
		}
			
	}
	
	public function getArticlesByCategoryName($filter = Null,$ignoreRequest = False) {
		
		/**
		 * Builds and initiates API request for Articles by articleCategoryName
		 *
		 * @param integer/string $filter - Filters request. Single i.e 1, Range i.e '1-3' or Selection i.e '1,2,4' 
		 * @param boolean $ignoreRequest - Used internally to prevent looking at $_REQUEST in some circumstances
		 */
		
		if($_REQUEST['title'] && $_REQUEST['aid'] && !$ignoreRequest) {
		
			return $this->getArticle($_REQUEST['aid']);
	
		} else {
		
			$apiCall = 'articles';
	
			if($filter) {
	
				$filter = '/articleCategoryName/'.$this->parseFilter($filter);
	
			}	

			$apiRequest = $this->requestUri.$apiCall.'/'.$this->apiKey.'/xml'.$filter;
	
			$apiResponse = $this->apiRequest($apiRequest);
	
			if($apiResponse) {				
	
				$xml = simplexml_load_string($apiResponse);
				return $this->articlesView($xml);				

			}
		
		}
			
	}
	
	
	protected function articlesView($xml) {
		
		/**
		 * Provides a HTML transformation of XML response for Articles
		 * 
		 * @param object $xml - XML response object
		 */
		
		$articles ='<div id="ap-articles-wrap">';
		
		$request = explode('?',$_SERVER['REQUEST_URI']);
		$scriptName = $request[0];
		
		foreach($xml->data->article as $article) {
			$articles.= '<div class="ap-article">';
			$articles.= '	<div class="ap-article-thumbnail">';
			$articles.= '		<a href="'.$scriptName.'?title='.urlencode($article->articleTitle).'&aid='.$article->articleID.'">';
			$articles.= '			<img src="'.$article->articleThumbnail.'" alt="'.$article->articleTitle.'" />';
			$articles.= '		</a>';
			$articles.= '	</div>';
			$articles.= '	<div class="ap-article-text">';
			$articles.= '		<div class="ap-article-titlelink">';
			$articles.= '			<h3>';
			$articles.= '				<a href="'.$scriptName.'?title='.urlencode($article->articleTitle).'&aid='.$article->articleID.'">'.$article->articleTitle.'</a>';
			$articles.= '			</h3>';
			$articles.= '		</div>';
			$articles.= '		<div class="ap-article-description">';
			$articles.= '			<p>';
			$articles.= 				$article->articleIntro;
			$articles.= '			</p>';			
			$articles.= '		</div>';			
			$articles.= '	</div>';
			$articles.= '</div>';
		}
		
		$articles.='</div>';
		return $articles;
		
	}
	
	
	public function getArticle($filter = Null) {
		
		/**
		 * Builds and initiates API request for Article
		 *
		 * @param integer/string $filter - Filters request. Single i.e 1, Range i.e '1-3' or Selection i.e '1,2,4' 
		 */
		
		if($_REQUEST['title'] && $_REQUEST['aid']) {
				
			$filter = $_REQUEST['aid'];
	
		}
		
		$apiCall = 'article';
	
		if($filter) {
		
			$filter = '/articleID/'.$filter;
		
		}	
	
		$apiRequest = $this->requestUri.$apiCall.'/'.$this->apiKey.'/xml'.$filter;
		$apiResponse = $this->apiRequest($apiRequest);
	
		if($apiResponse) {				
		
			$xml = simplexml_load_string($apiResponse);
			return $this->articleView($xml);				
	
		}
				
	}
	
	
	protected function articleView($xml) {
		
		/**
		 * Provides a HTML transformation of XML response for Article
		 * 
		 * @param object $xml - XML response object
		 */
		
		$articles ='<div id="ap-article-wrap">';
		
		$request = explode('?',$_SERVER['REQUEST_URI']);
		$scriptName = $request[0];
		
		foreach($xml->data->article as $article) {
			
			$articles .= $article->articleNarrative;
			
		}
		
		$articles.='</div>';
		return $articles;
		
	}	
	
	
	/**
	 * MyPortal
	 */
	
	public function getBusinessInformation() {
		
		/**
		 * Builds and initiates API request for Business Information
		 */
		
		$apiCall = 'business';

		$apiRequest = $this->requestUri.$apiCall.'/'.$this->apiKey.'/xml';
		$apiResponse = $this->apiRequest($apiRequest);

		if($apiResponse) {				

			$xml = simplexml_load_string($apiResponse);
			return $this->businessinformationView($xml);				

		}
		
	}	
	
	
	protected function businessinformationView($xml) {
		
		/**
		 * Provides a HTML transformation of XML response for Business (Information)
		 * 
		 * @param object $xml - XML response object
		 */
		
		$output = '<div id="ap-businessinformation-wrap">';
		
		foreach($xml->data->business as $business) {
			$output .= '<div class="ap-businessinformation">';
			$output .= '	<div class="ap-businessinformation-contactus">';
			$output .= '		<h4>Contact Us</h4>';
			$output .= '		<div class="ap-businessinformation-telephone">';
			$output .= '			<span class="ap-businessinformation-heading">Tel. </span>';
			$output .= 					$business->businessTelephone;
			$output .= '		</div>';
			if($business->businessFax != 0) {
			$output .= '		<div class="ap-businessinformation-fax">';
			$output .= '			<span class="ap-businessinformation-heading">Fax. </span>';
			$output .= 					$business->businessFax;
			$output .= '		</div>';
			}
			$output .= '		<div class="ap-businessinformation-email">';
			$output .= '			<span class="ap-businessinformation-heading">Email. </span>';
			$output .= 					$this->emailObfuscate($business->businessEmail);
			$output .= '		</div>';
			$output .= '	</div>';
			$output .= '	<div class="ap-businessinformation-principaladdress">';
			$output .= '		<h4>Principal Address</h4>';
			$output .= '		<div class="ap-businessinformation-address">';
			$output .= 				nl2br($business->businessPrincipalAddress);
			$output .= '			<br/>';
			$output .= 				$business->businessPostcode;
			$output .= '		</div>';
			$output .= '	</div>';
			
			if(($business->businessType == 'Limited Company') || ($business->buinessType == 'Limited Partnership')) {
				$output .= '<div class="ap-businessinformation-registeredoffice">';
				$output .= '	<h4>Registered Office</h4>';
				$output .= '	<div class="ap-businessinformation-address">';
				$output .= 			nl2br($business->businessRegisteredAddress);
				$output .= '	</div>';
				$output .= '</div>';
			}

			$output .= '	</div>';
			
			$output .= '	<div class="ap-businessinformation-legalstatus">';
		
			## Limited company, partership?
			if(($business->businessType == 'Limited Company') || ($business->buinessType == 'Limited Partnership')) {
				
				$statement = $business->businessName.' is a '. $business->businessType. ' registered in '. $business->businessRegisteredCountry. ' no. '. $business->businessCompanyNumber.'. ';
				
			} else {
			
				$statement = $business->businessName.' is a '. $business->businessType. '. ';
				
			}	
			
			## VAT registered
			
			if($business->businessVatNumber != '') {
			
				$vat = $business->businessName. ' is registered for VAT no. ' . $business->businessVatNumber . '.';
				
			} else {
			
				$vat = '';
				
			}			
			
			$output .= $statement.$vat;	
			$output .= '</div>';
			
			$output .= '<div class="ap-businessinformation-adviceareas">';
			
			$adviceareas = '<p>'.$business->businessName. ' is able to advise in the following areas.</p>';
			$adviceareas.= '<ul>';
			
			if ($business->businessAreasOfAdvice->mortgages) {
				$adviceareas.='<li>'.$business->businessAreasOfAdvice->mortgages.'</li>';
			}
			
			if ($business->businessAreasOfAdvice->protection) {
				$adviceareas.='<li>'.$business->businessAreasOfAdvice->protection.'</li>';
			}	
			
			if ($business->businessAreasOfAdvice->investments) {
				$adviceareas.='<li>'.$business->businessAreasOfAdvice->investments.'</li>';
			}
			
			if ($business->businessAreasOfAdvice->pensions) {
				$adviceareas.='<li>'.$business->businessAreasOfAdvice->pensions.'</li>';
			}
			
			if ($business->businessAreasOfAdvice->insurance) {
				$adviceareas.='<li>'.$business->businessAreasOfAdvice->insurance.'</li>';
			}
			
			if ($business->businessAreasOfAdvice->equityRelease) {
				$adviceareas.='<li>'.$business->businessAreasOfAdvice->equityRelease.'</li>';
			}
			
			if ($business->businessAreasOfAdvice->healthcare) {
				$adviceareas.='<li>'.$business->businessAreasOfAdvice->healthcare.'</li>';
			}
			
			if ($business->businessAreasOfAdvice->corporate) {
				$adviceareas.='<li>'.$business->businessAreasOfAdvice->corporate.'</li>';
			}
			
			if ($business->businessAreasOfAdvice->estatePlanning) {
				$adviceareas.='<li>'.$business->businessAreasOfAdvice->estatePlanning.'</li>';
			}
			
			$adviceareas .= '</ul>';
			
			$output .= $adviceareas;
			
			$output .= '</div>';
			
		}
		
		$output.= '</div>';	
		return $output;
			
	}	
	
	public function getOffices() {
		
		/**
		 * Builds and initiates API request for Offices 
		 */
	
		$apiCall = 'offices';

		$apiRequest = $this->requestUri.$apiCall.'/'.$this->apiKey.'/xml';
		$apiResponse = $this->apiRequest($apiRequest);

		if($apiResponse) {				

			$xml = simplexml_load_string($apiResponse);
			return $this->officesView($xml);				

		}
		
	}
	
	protected function officesView($xml) {
		
		/**
		 * Provides a HTML transformation of XML response for Offices (locations)
		 * 
		 * @param object $xml - XML response object
		 */
		
		$offices = '<div id="ap-officelocations-wrap">'; 
		
		foreach($xml->data->office as $office) {
			$offices .= '<div class="ap-officelocation">';
			$offices .= '	<h4 class="ap-officename">'.$office->officeName.'</h4>';
			$offices .= '	<div class="ap-officeaddress">'.nl2br($office->officeAddress).'</div>'; 
			$offices .= '	<div class="ap-officepostcode">'.$office->officePostcode.'</div>';
			
			if($office->officeTelephone != '') {
				$offices .= '<div class="ap-officetelephone"><span class="heading">Telephone</span>. '.$office->officeTelephone.'</div>';
			}	
			
			if($office->officeEmail != '') {
				$offices .= '<div class="ap-officeemail"><span class="heading">Email</span>. '.$this->emailObfuscate($office->officeEmail).'</div>';
			}	
		
			$offices .= '</div>';
			
		}
	
		$offices .= '</div>';
		return $offices;
	
	}
	
	
	public function getContacts() {
		
		/**
		 * Builds and initiates API request for Contacts (Contact list)
		 */
		
		$apiCall = 'contacts';

		$apiRequest = $this->requestUri.$apiCall.'/'.$this->apiKey.'/xml';
		$apiResponse = $this->apiRequest($apiRequest);

		if($apiResponse) {				

			$xml = simplexml_load_string($apiResponse);
			return $this->contactsView($xml);				

		}
		
	}	
	
	
	protected function contactsView($xml) {
		
		/**
		 * Provides a HTML transformation of XML response for Contacts
		 * 
		 * @param object $xml - XML response object
		 */
		
		$mobileCount = 0;
		
		foreach($xml->data->contact as $contact) {
			if($contact->peopleMobile != 0) {
				$mobileCount++;
			}
		}

		$contactList = '<div id="ap-contactlist-wrap">'; 
		$contactList.= '	<table width="100%" cellspacing="0" cellpadding="0" border="0">';
		$contactList.= '		<thead>';
		$contactList.= '			<tr>';
		$contactList.= '				<td class="ap-contactname">Name</td>';
		$contactList.= '				<td class="ap-contactposition">Position</td>';
		$contactList.= '				<td class="ap-contacttelephone">Telephone</td>';
		if($mobileCount != 0) {
		$contactList.= '				<td class="ap-contactmobile">Mobile</td>';
		}
		$contactList.= '				<td class="ap-contactemail">Email</td>';
		$contactList.= '			</tr>';
		$contactList.= '		</thead>';
		$contactList.= '		<tbody>';
		
		foreach($xml->data->contact as $contact) {
			$contactList.= '		<tr>';
			$contactList.= '			<td class="ap-contactname">'.$contact->peopleName.'</td>';
			$contactList.= '			<td class="ap-contactposition">'.$contact->peoplePosition.'</td>';
			$contactList.= '			<td class="ap-contacttelephone">'.$contact->peopleTelephone.'</td>';
			if($contact->peopleMobile != 0 || $mobileCount != 0) {
			$contactList.= '			<td class="ap-contactmobile">'.$contact->peopleMobile.'</td>';
			}
			$contactList.= '			<td class="ap-contactemail">'.$this->emailObfuscate($contact->peopleEmail).'</td>';
			$contactList.= '		</tr>';
		}
				
		$contactList .= '		</tbody>';	
		$contactList .= '	</table>';	
		$contactList .= '</div>';	
		
		return $contactList;
		
	}
	
	
	public function getBiographies() {
		
		/**
		 * Builds and initiates API request for Biographies
		 */
		
		$apiCall = 'biographies';

		$apiRequest = $this->requestUri.$apiCall.'/'.$this->apiKey.'/xml';
		$apiResponse = $this->apiRequest($apiRequest);

		if($apiResponse) {				

			$xml = simplexml_load_string($apiResponse);
			return $this->biographiesView($xml);				

		}
		
	}
	
	protected function biographiesView($xml) {
		
		/**
		 * Provides a HTML transformation of XML response for Biographies
		 * 
		 * @param object $xml - XML response object
		 */
		
		$bios = '<div id="ap-biographies-wrap">'; 

		
		foreach($xml->data->biography as $bio) {
			
			if($bio->peoplePicUri != '') {
				$bioPicHtml = '<div class="ap-biography-thumbnail">';
				$bioPicHtml.= '	<img src="'.$bio->peoplePicUri.'" alt="'.$bio->peopleName.'" />';
				$bioPicHtml.= '</div>';
			} else {
				$bioPicHtml = '<div class="ap-biography-no-thumbnail"></div>';
			}
			
			$contactInfo = '';
						
			if($bio->peopleTelephone != '') {
				$contactInfo .= '<span class="ap-biography-tel">Tel.</span> '.$bio->peopleTelephone;
			}
			
			if($bio->peopleMobile != '') {
				$contactInfo .= '<br/><span class="ap-biography-mobile">Mobile.</span> '.$bio->peopleMobile;
			}
			if($bio->peopleEmail != '') {
				$contactInfo .= '<br/><span class="ap-biography-email">Email.</span> '.$this->emailObfuscate($bio->peopleEmail);
			}			
			if($contactInfo !='') {
				$contactInfoHtml = '<div class="ap-biography-contact">'.$contactInfo.'</div>';
			} else {
				$contactInfoHtml = '';
			}		
	
			$bios .= '<div class="ap-biography">';
			$bios .= 	$bioPicHtml;
			$bios .= '	<div class="ap-biography-text">';
			$bios .= '		<div class="ap-biography-person">'.$bio->peopleName.', '.$bio->peoplePosition.'</div>';
			$bios .= 		$contactInfoHtml;
			$bios .= '		<div class="ap-biography-narrative">'.nl2br($bio->peopleBiography).'</div>';
			$bios .= '	</div>';
			$bios .= '</div>';
		
		}
	
		$bios .= '</div>';	
		return $bios;
	
	}
		

	public function getMortgageDisclosure($repossessionWarning = True, $fsaWarning = True) {
		
		/**
		 * Builds and initiates API request for Mortgage Disclosure
		 *
		 * @param boolean $repossessionWarning - Show repossession warning in disclosure. Default is True
		 * @param boolean $fsaWarning - Show FSA warning in disclosure. Default is True
		 */
		
		$apiCall = 'disclosures';

		$apiRequest = $this->requestUri.$apiCall.'/'.$this->apiKey.'/xml';
		$apiResponse = $this->apiRequest($apiRequest);

		if($apiResponse) {				

			$xml = simplexml_load_string($apiResponse);
			return $this->mortgagedisclosureView($xml, $repossessionWarning, $fsaWarning);			

		}
		
	}	
	
	
	protected function mortgagedisclosureView($xml, $repossessionWarning, $fsaWarning) {
		
		/**
		 * Provides a HTML transformation of XML response for Disclosures - Mortgages
		 * 
		 * @param object $xml - XML response object
		 * @param $respossessionWarning boolean - default (passed) is True
		 * @param $fsaWarning boolean - default (passed) is True
		 */
		
		$mortgageDisclosure = '<div class="ap-mortgage-disclosure-wrap">'; 

		foreach($xml->data->disclosure as $disclosure) {
					
			if($repossessionWarning) {
				if($disclosure->mortgagesRepossession !='') {
					$mortgageDisclosure .= '<div class="ap-mortgage-repossession-warning">'.$disclosure->mortgagesRepossession.'</div>';
				}
			}
			
			if($disclosure->mortgagesFee != '') {
				$mortgageDisclosure .= '<div class="ap-mortgage-fee-statement">'.nl2br($disclosure->mortgagesFee).'</div>';
			}
			
			if($fsaWarning) {
				if($disclosure->mortgagesFsaRegulation != '') {
					$mortgageDisclosure .= '<div class="ap-mortgage-fca-regulation">'.$disclosure->mortgagesFsaRegulation.'</div>';	
				}
			}	
			
		}
		
		$mortgageDisclosure .= '</div>';
		
		return $mortgageDisclosure;
	
	}
	
	
	public function getEquityReleaseDisclosure() {
		
		/**
		 * Builds and initiates API request for Equity Release Disclosure
		 */
		
		$apiCall = 'disclosures';

		$apiRequest = $this->requestUri.$apiCall.'/'.$this->apiKey.'/xml';
		$apiResponse = $this->apiRequest($apiRequest);

		if($apiResponse) {				

			$xml = simplexml_load_string($apiResponse);
			return $this->equityreleasedisclosureView($xml);				

		}
		
	}	
	
	
	protected function equityreleasedisclosureView($xml) {
		
		/**
		 * Provides a HTML transformation of XML response for Equity Release disclosure
		 * 
		 * @param object $xml - XML response object
		 */
		
		$erDisclosure = '<div class="ap-equity-release-disclosure-wrap">'; 

		foreach($xml->data->disclosure as $disclosure) {
			
			$erDisclosure .= nl2br($disclosure->equityRelease);
			
		}
		
		$erDisclosure .= '</div>';
		
		return $erDisclosure;
	
	}


	public function getFsaAuthorisationDisclosure() {
		
		/**
		 * Builds and initiates API request for FSA Authorisation Disclosure
		 */
		
		$apiCall = 'disclosures';

		$apiRequest = $this->requestUri.$apiCall.'/'.$this->apiKey.'/xml';
		$apiResponse = $this->apiRequest($apiRequest);

		if($apiResponse) {				

			$xml = simplexml_load_string($apiResponse);
			return $this->fsaAuthorisationDisclosureView($xml);				

		}
		
	}	
	
	
	protected function fsaAuthorisationDisclosureView($xml) {
		
		/**
		 * Provides a HTML transformation of XML response FSA Authorisation disclosure
		 * 
		 * @param object $xml - XML response object
		 */
		
		$fsaAuth = '<div class="ap-fca-disclosure-wrap">'; 

		foreach($xml->data->disclosure as $disclosure) {
			
			$fsaAuth .= nl2br($disclosure->fsaAuthorisation);
			
		}
		
		$fsaAuth .= '</div>';
		
		return $fsaAuth;
	
	}
	
	public function getExitSiteDisclosure() {
		
		/**
		 * Builds and initiates API request for Exit Site Disclosure
		 */
		
		$apiCall = 'disclosures';

		$apiRequest = $this->requestUri.$apiCall.'/'.$this->apiKey.'/xml';
		$apiResponse = $this->apiRequest($apiRequest);

		if($apiResponse) {				

			$xml = simplexml_load_string($apiResponse);
			return $this->exitSiteDisclosureView($xml);				

		}
		
	}	
	
	
	protected function exitSiteDisclosureView($xml) {
		
		/**
		 * Provides a HTML transformation of XML response Exit Site disclosure
		 * 
		 * @param object $xml - XML response object
		 */
		
		$exitSite = '<div class="ap-exit-site-disclosure-wrap">'; 

		foreach($xml->data->disclosure as $disclosure) {
			
			$exitSite .= $disclosure->exitSiteStatement;
			
		}
		
		$exitSite .= '</div>';
		
		return $exitSite;
	
	}
	
	
	
	protected function emailObfuscate($myEmail) {
		
		/**
		 * Helper method for obfuscating email addresses
		 * 
		 * @param string $myEmail - Email address
		 */
		
		if($myEmail != '') { 
			
			$myemailarray = explode("@",$myEmail);
			$mystring = "<script type=\"text/javascript\">\n";
			$mystring = $mystring . "<!--\n";
			$mystring = $mystring . "// Cloak Email Script, Ollie Phillips, Adviser Portals Ltd\n";
			$mystring = $mystring . "var cloak='@';\n";
			$mystring = $mystring . "cloak='" . $myemailarray[0] . "' + cloak;\n";
			$mystring = $mystring . "cloak+='" . $myemailarray[1] . "';\n";
			$mystring = $mystring . "document.write('<a href=\"mailto:' + cloak + '\">');\n//-->\n";
			$mystring = $mystring . "</script>\n";
			$mystring = $mystring . $myemailarray[0] . "<script type=\"text/javascript\">\n<!--\n";
			$mystring = $mystring . "document.write('@');\n//-->\n";
			$mystring = $mystring . "</script>" . $myemailarray[1] . "\n";
			$mystring = $mystring . "<script type=\"text/javascript\">\n";
			$mystring = $mystring . "<!--\n";
			$mystring = $mystring . "document.write('</a>');\n"; 
			$mystring = $mystring . "//--></script>";
        
			$cloak = $mystring;
			return $cloak; 
		
		} else {
		
			return false;
			
		}		
		
	}	


	protected function apiRequest($request, $cache = Null, $cacheExpireTime = Null) {
		
		/**
		 * Initiates Curl request to API
		 *
		 * @param string $request - Fully built request to API
		 * @param boolean $cache - Should cached response be used
		 * @param integer $cacheTime - How long in minutes should before cached response is refreshed
		 * @return string $apiXml - XML response from API
		 */
		
		## Get/Set Cache options
		if(!$cache) {
			
			$cache = $this->cacheResponses;
			
		}
		
		if(!$cacheExpireTime) {
			
			$cacheExpireTime = $this->cacheExpireMinutes;
			
		}		
	
		if($cache) {
			
			$filePath = $_SERVER['DOCUMENT_ROOT'].$this->cacheDirectory.'/';
			$fileName = md5($request);
			
			if(file_exists($filePath.$fileName)) {
				$requestTime = time();
				$expireTime = $cacheExpireTime * 60;
				$fileTime = filemtime($filePath.$fileName);
			}
			
			if(file_exists($filePath.$fileName) && (($requestTime - $expireTime) < $fileTime)) {
				
				if(($requestTime - $expireTime) < $fileTime) {
					
					$apiXml = file_get_contents($filePath.$fileName);
					$httpStatus = '200';
					
				}	
					
			} else {

				$apiXml = $this->curlConnect($request);
				if($apiXml) {
				
					file_put_contents($filePath.$fileName,$apiXml);
				
				} else {
					
					$apiXml = file_get_contents($filePath.$fileName);
					
				}
				
			}	
			
			return $apiXml;			
			
		} else {
			
			return $this->curlConnect($request);
			
		}			
		
	}
	
	
	protected function curlConnect($request) {
		
		/**
		 * Helper method which makes CURL call
		 * 
		 * @param string $request - Fully built request to API
		 */
		
		$curlHandle = curl_init();
        curl_setopt($curlHandle, CURLOPT_URL, $request);
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curlHandle, CURLOPT_HTTPHEADER, array('Expect:','Cache-Control: no-cache','Pragma: no-cache'));
        $apiXml = curl_exec($curlHandle);
        $httpStatus = curl_getinfo($curlHandle, CURLINFO_HTTP_CODE);
        curl_close($curlHandle);

		if($this->debugMode == True) {
			
			## Running in Debug Mode
			if($httpStatus == '200') {

				echo '<div><span style="font-weight:bold;">xml:</span><br/>'.htmlentities($apiXml).'<br/><br/></div>';
				return False;
				
			} else {

				echo  '<div><span style="font-weight:bold;">Error:</span> HTTP code '.$httpStatus.'<br/><br/></div>';
				return False;
			
			}
			
		} else {
			
			## Running in Production mode
			if($httpStatus == '200') {

				if($this->isXml($apiXml)) {

					return $apiXml;

				} else {

					return False;
				}

			} else {

				return False;

			}
					
		}
				
	}	
	
	
	protected function newsfeedRequest($rssUri) {

		/**
		 * Initiates Curl request to the selected RSS newsfeed
		 *
		 * @param string $rssUri - URI of RSS feed to get via CURL
		 * @return $string $newsfeedRss - RSS feed
		 */
		
		$curlHandle = curl_init();
        curl_setopt($curlHandle, CURLOPT_URL, $rssUri);
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curlHandle, CURLOPT_HTTPHEADER, array('Expect:','Cache-Control: no-cache','Pragma: no-cache'));
        $newsfeedRss = curl_exec($curlHandle);
        $httpStatus = curl_getinfo($curlHandle, CURLINFO_HTTP_CODE);
        curl_close($curlHandle);

		if($httpStatus == '200') {

			return $newsfeedRss;
			
		} else {

			echo  'Error: HTTP code '.$httpStatus;
			return False;
		
		}
		
	}
	
	
	protected function isXml($xml) {  
		
		/**
		 * Helper method to check we're not getting HTML in the response
		 *
		 * @param string $xml - XML response 	
		 * @return Boolean True or False
		 */
		
		$invalid= stripos($xml,'<html');

		if($invalid) {
		
			return False;
		
		} else {
			
			return True;
		
		}
		             
	}
	
	
	protected function parseFilter($filter) { 
		
		/**
		 * Helper method to construct an API ready request filter
		 *
		 * @param string $filter - filter from method call	
		 * @return string $apiFilter - api ready filter
		 */
		
		$apiFilter = str_replace(',','+',$filter);
		return $apiFilter;
		
	}
	
	
	protected function getExitSiteWarning() {
		
		/**
		 * Helper method to create Javascript Exit site warning
		 *
		 * @return string $eswScript - Javascript function
		 */
		
		$eswScript = "\n".'<script type="text/javascript">'."\n";
		$eswScript.= '// Adviser Portals'."\n"; 
		$eswScript.= '// Ollie Phillips - 08/02/12'."\n";
		$eswScript.= '// Site Exit Warning'."\n";
		$eswScript.= 'function apExitSiteWarning(){'."\n";
		$eswScript.= 'if(confirm("'.$this->exitSiteWarning.'")){'."\n";
		$eswScript.= 'return true;'."\n";
		$eswScript.= '} else {'."\n";
		$eswScript.= 'return false;'."\n";
		$eswScript.= '}'."\n";
		$eswScript.= '}'."\n";
		$eswScript.= '</script>'."\n";
		
		return $eswScript;
	
	}
	
	public function setApiKey($key) {
		$this->apiKey = $key;		
	}
	
}

class api extends adviserPortalsApi {
	
	public function setApiKey($key) {
		
		$this->apiKey = $key;
		
	}			
	
	public function setDebugMode() {
		
		$this->debugMode = True;
		
	}	
	
	public function setCacheResponses() {
		
		$this->cacheResponses = True;
		
	}
	
	public function setCacheExpireMinutes($minutes) {
		
		$this->cacheExpireMinutes = $minutes;
		
	}
	
	public function setCacheDirectory($directory) {
		
		$this->cacheDirectory = $directory;
		
	}
	
	public function getCacheDirectory() {
		
		return $this->cacheDirectory;
		
	}
	
	public function getCacheResponses() {
		
		return $this->cacheResponses;
		
	}
	
	public function getApiKey() {
		
		return $this->apiKey;
		
	}
	
	public function getCacheExpireMinutes() {
		
		return $this->cacheExpireMinutes;
		
	}
	
	public function getRequestUri() {
		
		return $this->requestUri;
		
	}
	
	public function ajaxCurlRequest($request) {
		
		return $this->curlConnect($request);
		
	}
	
	public function getDebugMode() {
		
		return $this->debugMode;
		
	}	

	/**
	 * Article Category Description
	 */
	
	public function getArticleCategoryDescription($filter = Null) {
		
		/**
		 * Builds and initiates API request for Article Category Description
		 *
		 * @param integer/string $filter - Filters request. Single i.e 1, Range i.e '1-3' or Selection i.e '1,2,4' 
		 * This is Concrete5 block specific method, not used in PHP class wrapper
		 */
		
		if($_REQUEST['aid']) {
		
			## Display nothing
	
		} else {
		
			$apiCall = 'articlecategories';
			
			if($filter) {
			
				$filter = '/articleCategoryID/'.$this->parseFilter($filter);
			
			}	
		
			$apiRequest = $this->requestUri.$apiCall.'/'.$this->apiKey.'/xml'.$filter;
			$apiResponse = $this->apiRequest($apiRequest);
		
			if($apiResponse) {				
			
				$xml = simplexml_load_string($apiResponse);
				return $this->articlecategorydescriptionView($xml);				
		
			}
		
		}	
			
	}
	
	
	protected function articlecategorydescriptionView($xml) {
		
		/**
		 * Provides a HTML transformation of XML response for Article Categories
		 * 
		 * @param object $xml - XML response object
		 */
		
		
		$request = explode('?',$_SERVER['REQUEST_URI']);
		$scriptName = $request[0];
		
		foreach($xml->data->articleCategory as $articleCategory) {
			
			$articleCategoryDesc.= '<div class="ap-category-description-wrap">';
			$articleCategoryDesc.= '	<h2>'.$articleCategory->articleCategoryName.'</h2>';
			$articleCategoryDesc.= '	<p>'.$articleCategory->articleCategoryDescription.'</p>';
			$articleCategoryDesc.= '	<hr/>';
			$articleCategoryDesc.= '</div>';
			
		}
		
		return $articleCategoryDesc;
		
	}
	
	public function getNotificationDetails() {
		
		/**
		 * Get Notifications Details
		 */

		$apiCall = 'notifications';

		$apiRequest = $this->requestUri.$apiCall.'/'.$this->apiKey.'/xml';
		$apiResponse = $this->apiRequest($apiRequest);

		if($apiResponse) {
			
			$js = Loader::helper('json');
			$apiResponse = str_replace('<![CDATA[','',$apiResponse);
			$apiResponse = str_replace(']]>','',$apiResponse);
			$xml = $this->objectToArray($js->decode($js->encode((array) simplexml_load_string($apiResponse)),1));
			$notify = array();
			foreach($xml['data']['compliance'] as $key => $value) {
				$notify[$key] = $value;
			}
			
			return $notify;				

		}
	
	}	
	
	protected function objectToArray($o) {
		
		if (is_object($o)) {
			$o = get_object_vars($o);
		}

		if (is_array($o)) {
			return array_map(array('api', 'objectToArray'), $o);
		} else {
			return $o;
		}
			
	}
	
	public function getBusinessName() {
		
		/**
		 * Builds and initiates API request for Business Name
		 */
		
		$apiCall = 'business';

		$apiRequest = $this->requestUri.$apiCall.'/'.$this->apiKey.'/xml';
		$apiResponse = $this->apiRequest($apiRequest);

		if($apiResponse) {	
			
			$bI = array();
			$xml = simplexml_load_string($apiResponse);
			$bI['name'] = $xml->data->business->businessName;
			$bI['tel'] = $xml->data->business->businessTelephone;
			$bI['email'] = $xml->data->business->businessEmail;
			$bI['address'] = $xml->data->business->businessPrincipalAddress;
			$bI['postcode'] = $xml->data->business->businessPostcode;
			$bI['mortgages'] = $xml->data->business->businessAreasOfAdvice->mortgages;
			$bI['pensions'] = $xml->data->business->businessAreasOfAdvice->pensions;
			return $bI;			

		}
		
	}
		
}	
?>