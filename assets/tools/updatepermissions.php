<?php
//Tools URL is ../index.php/tools/updatepermissions

/*
error_reporting(E_ALL);
ini_set('display_errors', 1);
*/

//Credentials
$username = 'temporary';
$email = 'temporary@adviserportals.co.uk';
$password = 'temporary';

//Create user and login
$u = new User();
if(!$u->getUserName($username)) {
$user = UserInfo::add([
	'uName' => $username,
	'uEmail' => $email,
	'uPassword' => $password,
	'uIsValidated' => 1
	]);
}

$userId = $user->getUserID($user);
User::loginByUserID($userId);

if($u->isLoggedIn()) {
	//User permissions
	$userPermissionsArray = array(54,55,56,57,59,60,61,62,72);
	foreach($userPermissionsArray as $key => $value) {
	    $permissions = PermissionKey::getList('user');
	    foreach($permissions as $pk) {
	        $value = $pk->getPermissionKeyID();
	        $pt = $pk->getPermissionAssignmentObject();
	        $pt->clearPermissionAssignment();
	        if ($value > 0 && in_array($value, $userPermissionsArray)) {
	            $pa = PermissionAccess::getByID($value, $pk);
	            if (is_object($pa)) {
	                $pt->assignPermissionAccess($pa);
	            }
	        }
	    }
	}
	//Page permissions
	$obj = new stdClass;
	$p = Page::getByID(1);
	$p->setPermissionsToManualOverride();
	$permissionArray = array(3);
	
	$pk = PermissionKey::getByHandle('add_subpage');
	$pk->setPermissionObject($p);
	$pt = $pk->getPermissionAssignmentObject();
	$pt->clearPermissionAssignment();
	$pa = PermissionAccess::create($pk);
	
	if (is_array($permissionArray)) {
		foreach($permissionArray as $gID) {
			$pa->addListItem(GroupPermissionAccessEntity::getOrCreate(Group::getByID($gID)));
		}
	}				
	$pt->assignPermissionAccess($pa);
}
$u->logout();
$ui = UserInfo::getByUserName($username);
$ui->delete();
header('Location: /');
?>