<?php  
	defined('C5_EXECUTE') or die("Access Denied.");
	class IfaportalsFeaturedContentBlockController extends BlockController {
	
		protected $btTable = 'btIfaportalsFeaturedContent';
		protected $btInterfaceWidth = "600";
		protected $btInterfaceHeight = "450";
		protected $btWrapperClass = 'ccm-ui';
		
		public function getBlockTypeDescription() {
			
			return t("A Featured content Block with several custom templates to enhance any IFA Portal website.");
			
		}
		
		public function getBlockTypeName() {
			
			return t("IFA Portals Featured Content");
			
		}
		
		public function on_page_view() {
			
			$this->addHeaderItem('<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Gochi+Hand" />');	
		
		}		
					
		public function view(){		
			
			## Deal with a width limit
			if($this->containerWidth) {
				
				$this->set('limitWidth', 'max-width:'.$this->containerWidth.'px;');
			
			}	
			
			## Get images if available
			$html = Loader::helper('html');
			
			$headerImg = $this->getHeaderImage();
			$mainImg = $this->getMainImage();

			if ($headerImg) {
				
				$this->set('headerImageHtml', $html->image($headerImg->getRelativePath()));
			
			}
			
			if ($mainImg) {
				
				$this->set('mainImageHtml', $html->image($mainImg->getRelativePath()));
				
			}
	
			$this->set('bID', $this->bID);
					
		}		
		
		public function save($data) {

			parent::save($data);

			$blockObject = $this->getBlockObject();
			if (is_object($blockObject)) {
				 
				$blockObject->setCustomTemplate($data['container']);
			 
			}
			
		}
		
		public function getHeaderImage() {

			if ($this->headerImage > 0) {
				
				return File::getByID($this->headerImage);
				
			}
			
			return false;
			
		}
		
		public function getMainImage() {
			
			if ($this->mainImage > 0) {
				
				return File::getByID($this->mainImage);
				
			}
			
			return false;
			
		}
		
		private function filterDirectories($value) {
			
	      	return ($value != '..' && $value != '.');
	
	    }
	
	 	private function getContainerByDirectory() {
		
	      	$containers = scandir(dirname(__FILE__) . '/templates');
	      	$containers = array_filter($containers, array($this,'filterDirectories'));
	      	return $containers;
	   
	 	}
     
	    public function getContainers() {
		
	      	$blockObject = $this->getBlockObject();
	
				if (is_object($blockObject)) {
					
					$bt = BlockType::getByID($blockObject->getBlockTypeID());
					return $bt->getBlockTypeCustomTemplates();
					
				}
				
	      	return $this->getContainerByDirectory();
	
	    }

	    public function getCurrentContainer() {
		
	      	$blockObject = $this->getBlockObject();

	      	if (is_object($blockObject)) {

				return $blockObject->getBlockFilename();
				
		 	}
				
			return '';
			
	    }	
		
	}
?>