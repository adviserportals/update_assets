<?php
defined('C5_EXECUTE') or die("Access Denied.");
class ApCallOutBlockController extends BlockController {
 
	protected $btTable = 'btApCallOut';
	protected $btInterfaceWidth = "610";
	protected $btInterfaceHeight = "500";
	//protected $btCacheBlockOutput = true;
	//protected $btCacheBlockOutputOnPost = true;
	//protected $btCacheBlockOutputForRegisteredUsers = true;
	protected $btWrapperClass = 'ccm-ui';
	
	private $searchableContent;
	
	public function getBlockTypeDescription() {
		
		return t("Add a Call Out to Your AdviserPortal Website");
		
	}
	
	public function getBlockTypeName() {
		
		return t("Call Out");
		
	}
		
	public function getSearchableContent(){
		
		return $this->searchableContent;
		
	}

	public function add() {
		$this->set('callOutTitleSize', 'h3');
		$this->set('callOutButtonText', 'More Info');
		$this->set('callOutBackgroundColor', '#666666');
		$this->set('callOutTitleColor', '#FFFFFF');
		$this->set('callOutTextColor', '#FFFFFF');
		$this->set('callOutButtonBackgroundColor', '#FFFFFF');
		$this->set('callOutButtonColor', '#000000');	
	}
	
	
	public function view() { 
	
		## Get images if available
		$html = Loader::helper('html');
		$callOutImg = $this->getHeaderImage();
		if ($callOutImg) {
			$this->set('callOutImageHtml', $html->image($callOutImg->getRelativePath()));		
		}
		
		if($this->callOutTextAlignment == 'left') {
			$buttonFloat = 'float:left;';
		} elseif($this->callOutTextAlignment == 'right') {
			$buttonFloat = 'float:right;';
		} elseif($this->callOutTextAlignment == 'center') {
			$buttonFloat = 'float:none;';
		}
		$this->set('buttonFloat', $buttonFloat);		
		
	}
	
	public function getHeaderImage() {
	
	if ($this->callOutImage > 0) {
		return File::getByID($this->callOutImage);
	}
	
	return false;
	
	}

	function getExternalLink() {return $this->externalLink;}
	function getInternalLinkCID() {return $this->internalLinkCID;}
	function getLinkURL() {
		if (!empty($this->externalLink)) {
			return $this->externalLink;
		} else if (!empty($this->internalLinkCID)) {
			$linkToC = Page::getByID($this->internalLinkCID);
			return (empty($linkToC) || $linkToC->error) ? '' : Loader::helper('navigation')->getLinkToCollection($linkToC);
		} else {
			return '';
		}
	}

	public function getJavaScriptStrings() {
		return array(
			'title-required' => t('You must enter a title.'),
		);
	}	

	public function save($args) {

		switch (intval($args['linkType'])) {
			case 1:
				$args['externalLink'] = '';
				break;
			case 2:
				$args['internalLinkCID'] = 0;
				break;
			default:
				$args['externalLink'] = '';
				$args['internalLinkCID'] = 0;
				break;
		}
		
		$args['enableLink'] = isset($args['enableLink']) ? 1 : 0;
		unset($args['linkType']); //this doesn't get saved to the database (it's only for UI usage)		
		parent::save($args);
		
	}

}
?>